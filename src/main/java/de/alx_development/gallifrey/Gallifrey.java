package de.alx_development.gallifrey;

/*-
 * #%L
 * Gallifrey
 * %%
 * Copyright (C) 2021 ALX-Development
 * %%
 * This file is part of the de.alx_development.gallifrey application.
 * 
 * The gallifrey application is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Diese Datei ist Teil der de.alx_development.gallifrey Anwendung.
 * 
 * Die Applikation ist Freie Software: Sie können sie unter den
 * Bedingungen der GNU Lesser General Public License, wie von der
 * Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl)
 * jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren.
 * 
 * Diese Applikation wird in der Hoffnung, dass sie nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 * 
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2021 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import de.alx_development.application.Bootstrapper;
import de.alx_development.application.JApplicationFrame;
import de.alx_development.gallifrey.tardis.*;

import javax.swing.*;
import java.awt.*;
import java.io.File;

public class Gallifrey extends JApplicationFrame
{
	private final TaskDetailView taskDetails = new TaskDetailView();

	/**
	 * The main function which is invoked if the application is started.
	 * The args parameter contains the supplied command-line arguments as an array of String objects.
	 * @param args command-line arguments as an array of Strings
	 */
	public static void main(String[] args)
	{
		Bootstrapper.load(Gallifrey.class);
	}
	
	public Gallifrey()
	{
		super();
		setTitle("Gallifrey");
		setApplicationIcon(new ImageIcon(Gallifrey.class.getResource("icon.png")));

		TaskListView taskListView = new TaskListView();
		// Displaying the basic information of the activated task in the info text line
		// at the bottom of the application
		taskListView.getTaskList().getListModel().addActivationChangeListener(event -> {
			try {
				Task task = event.getActivatedTask();

				setInfoicon(new ImageIcon(Task.class.getResource("timer-start.png")));
				setInfotext(String.format("Active task: <%s>", task.toString()));
			}
			catch(NullPointerException exc) {
				setInfoicon(new ImageIcon(Task.class.getResource("timer-pause.png")));
				setInfotext("Pause");
			}
		});

		// Implementing the T.A.R.D.I.S. manager for period mappings
		Tardis tardis = Tardis.getInstance();
		taskListView.getTaskList().getListModel().addActivationChangeListener(tardis);

		// Implementing a detail view and editor for the active selected
		// task in the task list.
		taskListView.getTaskList().addListSelectionListener(e -> {
			Task selectedTask = taskListView.getTaskList().getSelectedValue();
			taskDetails.setTask(selectedTask);
		});

		JTabbedPane jTabbedPane = new JTabbedPane(JTabbedPane.TOP,JTabbedPane.SCROLL_TAB_LAYOUT );
		jTabbedPane.addTab("Time schedule", new TimeSchedule());
		jTabbedPane.addTab("Task Information", taskDetails);

		// TODO: Replace Test environment with loadable list
		Task active = new Task("2", "Organisatorische Tätigkeit 2");

		taskListView.getTaskList().getListModel().addElement(new Task("0", "Testvorgang"));
		taskListView.getTaskList().getListModel().addElement(new Task("1", "Organisatorische Tätigkeit"));
		taskListView.getTaskList().getListModel().addElement(active);

		for(int i=3; i< 100; i++)
		{
			taskListView.getTaskList().getListModel().addElement(new Task(Integer.toString(i), "Noch eine Aufgabe"));
		}
		taskListView.getTaskList().getListModel().setActiveTask(active);

		// TODO: Testsequence
		Connector importer = new Importer(new File("/home/alex/workspace-git/gallifrey/data/2021-02-15 4210.txt"));

		JSplitPane splitPaneVert1;
		splitPaneVert1 = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, taskListView, jTabbedPane);
		splitPaneVert1.setOneTouchExpandable(true);

		add(splitPaneVert1, BorderLayout.CENTER);
	}

	/**
	 * Terminates the currently running Application and is closing any external
	 * connections.
	 *
	 * @param status exit status.
	 */
	@Override
	public void exit(int status) {

		// Closing the Tardis instance
		Tardis.getInstance().close();

		// Doing everything the application itself needs to do
		super.exit(status);
	}
}
