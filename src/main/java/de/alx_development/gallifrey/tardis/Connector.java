package de.alx_development.gallifrey.tardis;

import javax.swing.*;

public interface Connector {

    int getId();

    String getName();

    ImageIcon getImageIcon();

    boolean isWritable();
}
