package de.alx_development.gallifrey.tardis;

/*-
 * #%L
 * Gallifrey
 * %%
 * Copyright (C) 2021 ALX-Development
 * %%
 * This file is part of the de.alx_development.gallifrey application.
 * 
 * The gallifrey application is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Diese Datei ist Teil der de.alx_development.gallifrey Anwendung.
 * 
 * Die Applikation ist Freie Software: Sie können sie unter den
 * Bedingungen der GNU Lesser General Public License, wie von der
 * Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl)
 * jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren.
 * 
 * Diese Applikation wird in der Hoffnung, dass sie nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 * 
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2021 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

/**
 * A special EventObject to notify a <code>TaskActivationListener</code> about an activation change and the previous
 * activated task. To this end, this <code>TaskActivationEvent</code> contains a pointer to both the actual activated
 * task and the previous active task.
 */
public class TaskActivationEvent extends java.util.EventObject {

    private final Task deactivatedTask;
    private final Task activatedTask;

    /**
     * Constructs a prototypical Event.
     *
     * @param source the object on which the Event initially occurred
     * @param activatedTask The new activated task
     * @param deactivatedTask The former active task
     * @throws IllegalArgumentException if source is null
     */
    public TaskActivationEvent(TaskListModel source, Task deactivatedTask, Task activatedTask) {
        super(source);
        this.deactivatedTask = deactivatedTask;
        this.activatedTask = activatedTask;
    }

    /**
     * Use this function to get access to the previous active task. If the activation
     * of a task start without a former active task (i.e., initial activation) this
     * function returns a null reference.
     *
     * @return Reference to the former active task
     */
    public Task getDeactivatedTask() {
        return this.deactivatedTask;
    }

    /**
     * Using this function will provide access to the activated task. If no new
     * task has been activated (i.e., stopping the activation) a null reference
     * will be returned.
     *
     * @return A reference to the activated task
     */
    public Task getActivatedTask() {
        return this.activatedTask;
    }

    /**
     * The <code>TaskListModel</code> on which the Event initially occurred.
     *
     * @return the object on which the Event initially occurred
     */
    @Override
    public TaskListModel getSource() {
        return (TaskListModel) super.getSource();
    }
}
