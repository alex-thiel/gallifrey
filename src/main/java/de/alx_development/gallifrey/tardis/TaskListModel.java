package de.alx_development.gallifrey.tardis;

/*-
 * #%L
 * Gallifrey
 * %%
 * Copyright (C) 2021 ALX-Development
 * %%
 * This file is part of the de.alx_development.gallifrey application.
 * 
 * The gallifrey application is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Diese Datei ist Teil der de.alx_development.gallifrey Anwendung.
 * 
 * Die Applikation ist Freie Software: Sie können sie unter den
 * Bedingungen der GNU Lesser General Public License, wie von der
 * Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl)
 * jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren.
 * 
 * Diese Applikation wird in der Hoffnung, dass sie nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 * 
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2021 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import de.alx_development.application.Application;

import javax.swing.*;
import javax.swing.event.EventListenerList;
import java.util.Enumeration;
import java.util.Vector;

/**
 * This class implements the list model which handles the
 * tardis as list for display issues.
 */
public class TaskListModel extends AbstractListModel<Task> {

    private final Vector<Task> listVector = new Vector<>();

    private Task activeTask = null;
    private Task lastActiveTask = null;

    /**
     * List of listeners
     */
    protected EventListenerList listenerList = new EventListenerList();

    /**
     * Returns the number of components in this list.
     *
     * @return  the number of components in this list
     */
    public int getSize() {
        return listVector.size();
    }

    /**
     * This function returns the actual active task. The active
     * task is the task on which the actual time operation is done.
     *
     * @return the active task object
     */
    public Task getActiveTask() {
        return activeTask;
    }

    /**
     * This function returns the formerly active task. This is useful
     * if the actual activated task is a null reference
     *
     * @return The formerly active task
     */
    public Task getLastActiveTask() {
        return lastActiveTask;
    }

    /**
     * Use this function to set up a new task as active task.
     *
     * @param activeTask
     */
    public void setActiveTask(Task activeTask) {
        Task deactivatedTask = this.activeTask;

        if(deactivatedTask != activeTask) {
            this.lastActiveTask = deactivatedTask;
            this.activeTask = activeTask;

            fireContentsChanged(this, indexOf(activeTask), indexOf(activeTask));
            fireTaskActivationChanged(deactivatedTask, activeTask);
            Application.logger.fine(String.format("Task <%s> activated", (activeTask == null) ? "null" : activeTask.toString()));
        }
    }

    /**
     * Returns the component at the specified index.
     *
     * @param      index   an index into this list
     * @return     the component at the specified index
     * @throws     ArrayIndexOutOfBoundsException  if the {@code index} is negative or greater than the current size of this list
     */
    public Task getElementAt(int index) {
        return listVector.elementAt(index);
    }

    /**
     * Tests whether this list has any components.
     *
     * @return  {@code true} if and only if this list has no components, that is, its size is zero; {@code false} otherwise
     */
    public boolean isEmpty() {
        return listVector.isEmpty();
    }

    /**
     * Returns an enumeration of the components of this list.
     *
     * @return  an enumeration of the components of this list
     */
    public Enumeration<Task> elements() {
        return listVector.elements();
    }

    /**
     * Tests whether the specified object is a component in this list.
     *
     * @param   task   a task object
     * @return  {@code true} if the specified task is the same as a component in this list
     */
    public boolean contains(Task task) {
        return listVector.contains(task);
    }

    /**
     * Searches for the first occurrence of {@code task}.
     *
     * @param   task   an object
     * @return  the index of the first occurrence of the argument in this list; returns {@code -1} if the object is not found
     */
    public int indexOf(Task task) {
        return listVector.indexOf(task);
    }

    /**
     * Searches for the first occurrence of {@code task}, beginning the search at {@code index}.
     *
     * @param   task    the desired component
     * @param   index   the index from which to begin searching
     * @return  the index where the first occurrence of {@code task}
     *          is found after {@code index}; returns {@code -1}
     *          if the {@code task} is not found in the list
     */
    public int indexOf(Task task, int index) {
        return listVector.indexOf(task, index);
    }

    /**
     * Returns the index of the last occurrence of {@code task}.
     *
     * @param   task   the desired component
     * @return  the index of the last occurrence of {@code task}
     *          in the list; returns {@code task} if the object is not found
     */
    public int lastIndexOf(Task task) {
        return listVector.lastIndexOf(task);
    }

    /**
     * Searches backwards for {@code task}, starting from the specified index, and returns an index to it.
     *
     * @param  task    the desired component
     * @param  index   the index to start searching from
     * @return the index of the last occurrence of the {@code task}
     *          in this list at position less than {@code index};
     *          returns {@code -1} if the object is not found
     */
    public int lastIndexOf(Task task, int index) {
        return listVector.lastIndexOf(task, index);
    }

    /**
     * Returns the component at the specified index.
     * <blockquote>
     * <b>Note:</b> Although this method is not deprecated, the preferred
     *    method to use is {@code get(int)}, which implements the
     *    {@code List} interface defined in the 1.2 Collections framework.
     * </blockquote>
     *
     * @param      index   an index into this list
     * @return     the component at the specified index
     * @see Vector#elementAt(int)
     */
    public Task elementAt(int index) {
        return listVector.elementAt(index);
    }

    /**
     * Returns the first component of this list.
     * @return     the first component of this list
     */
    public Task firstElement() {
        return listVector.firstElement();
    }

    /**
     * Returns the last component of the list.
     *
     * @return  the last component of the list
     */
    public Task lastElement() {
        return listVector.lastElement();
    }

    /**
     * Sets the component at the specified {@code index} of this
     * list to be the specified task. The previous component at that
     * position is discarded.
     *
     * @param      task what the component is to be set to
     * @param      index   the specified index
     */
    public void setElementAt(Task task, int index) {
        listVector.setElementAt(task, index);
        fireContentsChanged(this, index, index);
    }

    /**
     * Deletes the component at the specified index.
     *
     * @param      index   the index of the object to remove
     */
    public void removeElementAt(int index) {
        listVector.removeElementAt(index);
        fireIntervalRemoved(this, index, index);
    }

    /**
     * Inserts the specified task as a component in this list at the
     * specified <code>index</code>.
     *
     * @param      task the component to insert
     * @param      index   where to insert the new component
     */
    public void insertElementAt(Task task, int index) {
        listVector.insertElementAt(task, index);
        fireIntervalAdded(this, index, index);
    }

    /**
     * Adds the specified component to the end of this list.
     *
     * @param   task   the component to be added
     */
    public void addElement(Task task) {
        int index = listVector.size();
        listVector.addElement(task);
        fireIntervalAdded(this, index, index);
    }

    /**
     * Removes the first (lowest-indexed) occurrence of the argument
     * from this list.
     *
     * @param   task   the component to be removed
     * @return  {@code true} if the argument was a component of this list; {@code false} otherwise
     */
    public boolean removeElement(Task task) {
        int index = indexOf(task);
        boolean rv = listVector.removeElement(task);
        if (index >= 0) {
            fireIntervalRemoved(this, index, index);
        }
        return rv;
    }

    /**
     * Removes all components from this list and sets its size to zero.
     */
    public void removeAllElements() {
        int index1 = listVector.size()-1;
        listVector.removeAllElements();
        if (index1 >= 0) {
            fireIntervalRemoved(this, 0, index1);
        }
    }

    /**
     * Returns a string that displays and identifies this
     * object's properties.
     *
     * @return a String representation of this object
     */
    public String toString() {
        return listVector.toString();
    }

    /**
     * Notifies all listeners that have registered interest for
     * notification on this event type.  The event instance
     * is lazily created using the parameters passed into
     * the fire method.
     *
     * @param deactivatedTask the former activated task
     * @param activatedTask the actual activated task
     * @see EventListenerList
     */
    protected void fireTaskActivationChanged(Task deactivatedTask, Task activatedTask) {
        TaskActivationListener[] listeners = listenerList.getListeners(TaskActivationListener.class);
        TaskActivationEvent e = new TaskActivationEvent(this, deactivatedTask, activatedTask);
        // Process the listeners last to first, notifying
        // those that are interested in this event
        for (int i = listeners.length - 1; i >= 0; --i) {
            listeners[i].taskActivated(e);
        }
    }

    /**
     * If the active task changes it is possible to be notified by the model using
     * a <code>TaskActivationChangeListener</code>
     *
     * @param taskActivationListener The listener to add
     */
    public synchronized void addActivationChangeListener(TaskActivationListener taskActivationListener) {
        listenerList.add(TaskActivationListener.class, taskActivationListener);
    }

    /**
     * If a listener should no longer be triggered by activation changes it may be
     * removed from the listener list
     *
     * @param taskActivationListener The listener to remove
     */
    public synchronized void removeActivationChangeListener(TaskActivationListener taskActivationListener) {
        listenerList.remove(TaskActivationListener.class, taskActivationListener);
    }
}
