package de.alx_development.gallifrey.tardis;

import javax.swing.*;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Vector;
import java.util.logging.Logger;

public class Importer implements Connector {

    private final ImageIcon imageIcon;
    private final File sourceFile;
    private Charset charset;
    private final Vector<Task> taskList;

    protected Logger logger;

    {
        logger = Logger.getLogger(getClass().getName());
        imageIcon = new ImageIcon(Task.class.getResource("task_type-sap.png"));
        taskList = new Vector<>();
    }

    public Importer(File sourceFile) {
        this(sourceFile, StandardCharsets.ISO_8859_1);
    }

    public Importer(File sourceFile, Charset charset) {
        this.sourceFile = sourceFile;
        this.charset = charset;

        logger.finest(String.format("Connector %s initialized with ID: %s", getName(), getId()));

        // Reading the tasks from file
        readTasks();
    }

    public int getId() {
        return sourceFile.getAbsolutePath().hashCode();
    }

    public String getName() {
        return String.format("SAP Importer [%s]", sourceFile.getAbsolutePath());
    }

    public ImageIcon getImageIcon() {
        return imageIcon;
    }

    public boolean isWritable() {
        return sourceFile.canWrite();
    }

    public void setCharset(Charset charset) {
            this.charset = charset;
    }

    public Charset getCharset() {
        // Avoid returning an undefined charset
        return charset != null ? charset : StandardCharsets.ISO_8859_1;
    }

    public void readTasks() {

        String regexp = "(\\|\\s*([a-zA-Z0-9üöäÜÖÄß ._-]+)\\s*)+.*";

        logger.finest(String.format("Reading task content from file [%s]", sourceFile));

        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(sourceFile, getCharset()));
            String[] header = null;
            String lineString;
            while ( (lineString = bufferedReader.readLine()) != null ) {
                if(lineString.matches(regexp)) {
                    String[] lineSegments = lineString.split("\\s*\\|\\s*");
                    //The first element is an empty one cause the line starts with the delimiter
                    String[] elements = Arrays.copyOfRange(lineSegments, 1, lineSegments.length);

                    // First matching line is treated as header
                    if(header == null) {
                        header = elements;
                    }
                    //Any other valid line is expected to be a task
                    else {
                        //System.out.println(Arrays.toString(elements));

                        Task task = new Task(){
                            public String getReferenceCode() {
                                return String.format("%s#%s", getProperty("PLAN ID"), getProperty("ERSTER VOR"));
                            }
                            public String getName() {
                                return getProperty("KURZTEXT AUFTRAG");
                            }
                            public Connector getConnector() {
                                return Importer.this;
                            }
                        };

                        for(int i = 0; i < elements.length; i++) {
                            task.setProperty(header[i].toUpperCase(),elements[i]);
                        }

                        System.out.println("Task " + task.getReferenceCode() + " -> " + task.getName());
                    }
                }
            }

            bufferedReader.close();

        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

}
