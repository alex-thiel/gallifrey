package de.alx_development.gallifrey.tardis;

import de.alx_development.application.languages.Translator;

import javax.swing.table.AbstractTableModel;
import java.util.logging.Logger;

/**
 * The <code>PreferenceTableModel</code> specifies the methods the
 * <code>JTable</code> will use to interrogate with the preferences data.
 */
class TaskPropertiesTableModel extends AbstractTableModel {

    /**
     * Defining the column names using the translation functionality from the application
     * framework.
     */
    private static final String[] COLUMN_NAMES = {
            Translator.getInstance().getLocalizedString("PROPERTY"),
            Translator.getInstance().getLocalizedString("PROPERTY_VALUE")
    };

    /**
     * Internal logger instance to handle log messages in a unique way
     */
    private static final Logger logger = Logger.getLogger(TaskPropertiesTableModel.class.getName());

    private final Task task;

    /**
     * The default constructor which expects the task to be displayed as parameter
     *
     * @param task The task which should be represented by this model
     */
    TaskPropertiesTableModel(Task task) {
        super();
        this.task = task;
    }

    /**
     * This function returns the reference to the actual task assigned to
     * the table model.
     *
     * @return The actual assigned task
     */
    public Task getTask() {
        return this.task;
    }

    /**
     * Returns the number of rows in the model. A
     * <code>JTable</code> uses this method to determine how many rows it
     * should display.  This method should be quick, as it
     * is called frequently during rendering.
     *
     * @return the number of rows in the model
     * @see #getColumnCount
     */
    public int getRowCount() {
        return getTask().getPropertiesCount();
    }

    /**
     * Returns the number of columns in the model. A
     * <code>JTable</code> uses this method to determine how many columns it
     * should create and display by default.
     *
     * @return the number of columns in the model
     * @see #getRowCount
     */
    @Override
    public int getColumnCount() {
        return COLUMN_NAMES.length;
    }

    /**
     * Returns the name of the column at <code>columnIndex</code>.  This is used
     * to initialize the table's column header name.  Note: this name does
     * not need to be unique; two columns in a table can have the same name.
     *
     * @param columnIndex the index of the column
     * @return the name of the column
     */
    @Override
    public String getColumnName(int columnIndex) {
        return COLUMN_NAMES[columnIndex];
    }

    /**
     * Returns the value for the cell at <code>columnIndex</code> and
     * <code>rowIndex</code>.
     *
     * @param rowIndex    the row whose value is to be queried
     * @param columnIndex the column whose value is to be queried
     * @return the value Object at the specified cell
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        String[] keys = getPropertyKeys();
        String value = null;

        switch (columnIndex) {
            case 0:
                value = keys[rowIndex];
                break;
            case 1:
                value = task.getProperty(keys[rowIndex], "N/A");
                break;
        }

        return value;
    }

    /**
     * This function returns an array containing the keys of all
     * properties assigned to the task.
     *
     * @return String array of all property keys
     */
    private String[] getPropertyKeys() {
        String[] keys = task.getPropertyNames().toArray(new String[0]);
        return keys;
    }

    /**
     * Returns true if the cell at rowIndex and columnIndex is editable. Otherwise, setValueAt on the cell
     * will not change the value of that cell.
     * In case of this model this function always returns false.
     *
     * @param rowIndex    the row whose value to be queried
     * @param columnIndex the column whose value to be queried
     * @return true if the cell is editable
     * @see #setValueAt(Object, int, int)
     */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }
}
