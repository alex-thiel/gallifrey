package de.alx_development.gallifrey.tardis;

/*-
 * #%L
 * Gallifrey
 * %%
 * Copyright (C) 2021 ALX-Development
 * %%
 * This file is part of the de.alx_development.gallifrey application.
 * 
 * The gallifrey application is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Diese Datei ist Teil der de.alx_development.gallifrey Anwendung.
 * 
 * Die Applikation ist Freie Software: Sie können sie unter den
 * Bedingungen der GNU Lesser General Public License, wie von der
 * Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl)
 * jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren.
 * 
 * Diese Applikation wird in der Hoffnung, dass sie nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 * 
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2021 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import de.alx_development.gallifrey.tardis.Task;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Logger;

public class TimePeriod {

    protected Logger logger = Logger.getLogger(getClass().getName());
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");

    private final Task task;
    private final LocalDateTime startTime;
    private LocalDateTime stopTime;

    protected TimePeriod(Task task) {
        this.task = task;
        this.startTime = LocalDateTime.now();
        logger.fine(String.format("New time period initialized Task:[%s] Start:[%s]", (getTask() == null) ? "Pause" : getTask().toString(), getStartTime().format(formatter)));
    }

    public Task getTask() {
        return task;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public LocalDateTime getStopTime() {
        return stopTime;
    }

    public Duration getDuration() {
        LocalDateTime stopTime = (getStopTime() == null) ? LocalDateTime.now() : getStopTime();
        Duration duration = Duration.between(getStartTime(), stopTime);
        return duration;
    }

    protected void stop() {
        this.stopTime = LocalDateTime.now();

        logger.fine(String.format("Time period finalized Task:[%s] Start:[%s] Stop:[%s] Duration:[%d SEC]",
                getTask().toString(),
                getStartTime().format(formatter),
                getStopTime().format(formatter),
                getDuration().getSeconds()));
    }
}
