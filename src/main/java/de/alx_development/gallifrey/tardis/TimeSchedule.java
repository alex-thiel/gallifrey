package de.alx_development.gallifrey.tardis;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import java.awt.*;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class TimeSchedule extends JPanel  {

    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEE dd.MM.yyyy");
    private LocalDateTime day = LocalDateTime.now();

    private final JEditorPane ep = new JEditorPane();
    private final JLabel dayLabel = new JLabel();

    public TimeSchedule() {
        super();
        setLayout(new BorderLayout());

        // Toolbar
        JButton toDayButton = new JButton(new ImageIcon(getClass().getResource("calendar-today.png")));
        toDayButton.setToolTipText("Heute");
        toDayButton.addActionListener(e -> setDay(LocalDateTime.now()));
        JButton previousWeekButton = new JButton(new ImageIcon(getClass().getResource("go-previous-skip.png")));
        previousWeekButton.setToolTipText("Vorherige Woche");
        previousWeekButton.addActionListener(e -> setDay(day.minusDays(7)));
        JButton previousDayButton = new JButton(new ImageIcon(getClass().getResource("go-previous.png")));
        previousDayButton.setToolTipText("Vorheriger Tag");
        previousDayButton.addActionListener(e -> setDay(day.minusDays(1)));
        dayLabel.setBorder(new EtchedBorder(EtchedBorder.LOWERED));
        dayLabel.setFont(new Font(dayLabel.getFont().getName(), Font.BOLD, 20));
        JButton nextDayButton = new JButton(new ImageIcon(getClass().getResource("go-next.png")));
        nextDayButton.setToolTipText("Nächste Tag");
        nextDayButton.addActionListener(e -> setDay(day.plusDays(1)));
        JButton nextWeekButton = new JButton(new ImageIcon(getClass().getResource("go-next-skip.png")));
        nextWeekButton.setToolTipText("Nächste Woche");
        nextWeekButton.addActionListener(e -> setDay(day.plusDays(7)));

        final JToolBar toolBar = new JToolBar("Time control panel");
        toolBar.setBorderPainted(true);
        toolBar.add(toDayButton);
        toolBar.addSeparator();
        toolBar.add(previousWeekButton);
        toolBar.add(previousDayButton);
        toolBar.add(dayLabel);
        toolBar.add(nextDayButton);
        toolBar.add(nextWeekButton);

        // Time schedule
        ep.setContentType("text/html");
        setDay(LocalDateTime.now());

        // TODO: Implement a table model for the time schedule
        JTable table = new JTable(new TimeScheduleModel());
        table.setDefaultRenderer(Task.class, new TaskCellRenderer());
        table.setDefaultRenderer(LocalDateTime.class, new LocalDateTimeCellRenderer());
        table.setDefaultRenderer(Duration.class, new DurationCellRenderer());

        // Setting up the enclosing scroll pane
        JScrollPane jScrollPane = new JScrollPane();
        jScrollPane.setBounds(new Rectangle(40, 30, 150, 200));
        jScrollPane.getViewport().add(table, null);

        add(toolBar, BorderLayout.NORTH);
        add(jScrollPane, BorderLayout.CENTER);
    }

    public LocalDateTime getDay() {
        return day;
    }

    public void setDay(LocalDateTime day) {
        this.day = day;

        dayLabel.setText(day.format(formatter));
        // TODO: Updating the time schedule table model
        ep.setText(Tardis.getInstance().getHTMLString());
    }
}
