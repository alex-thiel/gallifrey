package de.alx_development.gallifrey.tardis;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.time.Duration;

public class DurationCellRenderer implements TableCellRenderer {

    private final JLabel renderer = new JLabel();

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

        // Must be triggered to get the <code>setBackground</code> command effective
        renderer.setOpaque(true);

        // Task is actually selected
        if(isSelected){
            renderer.setForeground(UIManager.getColor("List.selectionForeground"));
            renderer.setBackground(UIManager.getColor("List.selectionBackground"));
        }
        // Task is not selected
        else{
            renderer.setForeground(UIManager.getColor("List.foreground"));
            renderer.setBackground(UIManager.getColor("List.background"));
        }

        try {
            // Casting the value to date time object
            Duration duration = (Duration) value;
            String s = String.format("%02d:%02d:%02d", duration.toHours(), duration.toMinutesPart(), duration.toSecondsPart());
            String h = String.format("%d,%02d h", duration.toHours(), (int)((double)duration.toMinutesPart() / 0.6));
            renderer.setHorizontalAlignment(SwingConstants.RIGHT);
            renderer.setText("<html><p>"+s+"</p><p><small>"+h+"</small></p></html>");
        }
        catch(NullPointerException exception) {
            renderer.setHorizontalAlignment(SwingConstants.CENTER);
            renderer.setText("<html><p style='text-align:center;'>Running...</p></html>");
        }

        return renderer;
    }
}
