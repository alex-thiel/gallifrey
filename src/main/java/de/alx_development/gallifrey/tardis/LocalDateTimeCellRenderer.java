package de.alx_development.gallifrey.tardis;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class LocalDateTimeCellRenderer implements TableCellRenderer {

    private final DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");
    private final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    private final JLabel renderer = new JLabel();

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

        // Must be triggered to get the <code>setBackground</code> command effective
        renderer.setOpaque(true);

        // Task is actually selected
        if(isSelected){
            renderer.setForeground(UIManager.getColor("List.selectionForeground"));
            renderer.setBackground(UIManager.getColor("List.selectionBackground"));
        }
        // Task is not selected
        else{
            renderer.setForeground(UIManager.getColor("List.foreground"));
            renderer.setBackground(UIManager.getColor("List.background"));
        }

        try {
            // Casting the value to date time object
            LocalDateTime dateTime = (LocalDateTime) value;
            renderer.setHorizontalAlignment(SwingConstants.RIGHT);
            renderer.setIcon(null);
            renderer.setText("<html><p>"+dateTime.format(timeFormatter)+"</p><p><small>"+dateTime.format(dateFormatter)+"</small></p></html>");
        }
        catch(NullPointerException exception) {
            renderer.setHorizontalAlignment(SwingConstants.CENTER);
            renderer.setIcon(new ImageIcon(Task.class.getResource("task-running.png")));
            renderer.setText("<html><p style='text-align:center;'>Running...</p></html>");
        }

        return renderer;
    }
}
