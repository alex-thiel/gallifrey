package de.alx_development.gallifrey.tardis;

/*-
 * #%L
 * Gallifrey
 * %%
 * Copyright (C) 2021 ALX-Development
 * %%
 * This file is part of the de.alx_development.gallifrey application.
 * 
 * The gallifrey application is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Diese Datei ist Teil der de.alx_development.gallifrey Anwendung.
 * 
 * Die Applikation ist Freie Software: Sie können sie unter den
 * Bedingungen der GNU Lesser General Public License, wie von der
 * Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl)
 * jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren.
 * 
 * Diese Applikation wird in der Hoffnung, dass sie nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 * 
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2021 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import javax.swing.*;
import java.util.HashMap;
import java.util.Set;

/**
 * A task represents an activity that needs to be accomplished within
 * a defined period of time or by a deadline. The task object has no
 * information about the spend time on it.
 */
public class Task {

    public static final ImageIcon TYPE_UNKNOWN = new ImageIcon(Task.class.getResource("task_type-unknown.png"));
    public static final ImageIcon TYPE_SAP = new ImageIcon(Task.class.getResource("task_type-sap.png"));

    private final ImageIcon type = Task.TYPE_SAP;

    private final HashMap<String, String> properties = new HashMap<>();

    public Task() {

    }

    //TODO: Check if this function is necessary
    public ImageIcon getType() {
        return type;
    }

    /**
     * A task will be represented by a reference code (i.e., the task number or
     * a number for time logging) which identifies it in the system where the
     * timing is logged and a name for simpler identification by the human users.
     *
     * @param referenceCode A reference code to identify the task in foreign systems
     * @param name A name for human readable display
     */
    public Task(String referenceCode, String name) {
        super();

        setProperty("REFERENCE", referenceCode);
        setProperty("NAME", name);
    }

    /**
     * This function returns the reference code which is assigned to this task
     * object.
     *
     * @return The reference code of the task
     */
    public String getReferenceCode() {
        return getProperty("REFERENCE");
    }

    /**
     * This function returns the given name for the task
     *
     * @return The name of the task
     */
    public String getName() {
        return getProperty("NAME");
    }

    /**
     * Associate value with key. Returns the previous value associated with key,
     * or returns null if no such association exist.
     *
     * @param key   The property key
     * @param value The new value for the property.
     * @return The previous value of the specified key in this property list, or null if it did not have one.
     */
    public String setProperty(String key, String value) {
        return properties.put(key, value);
    }

    /**
     * This method returns an enumeration of the keys.
     *
     * @return Enumeration of available keys
     */
    public Set<String> getPropertyNames() {
        return properties.keySet();
    }

    /**
     * Searches for the property with the specified key in this property list.
     * If the key is not found in this property list, the default property list,
     * and its defaults, recursively, are then checked.
     * The method returns null if the property is not found.
     *
     * @param key The property key to look for.
     * @return The value associated with the key.
     */
    public String getProperty(String key) {
        return properties.get(key);
    }

    /**
     * Searches for the property with the specified key in this property list.
     * If the key is not found in this property list, the default property list,
     * and its defaults, recursively, are then checked.
     * The method returns the default value argument if the property is not found.
     * In difference to the java implementation of a properties list the default value
     * is associated with the key and stored permanently in the properties list.
     *
     * @param key          The property key to look for.
     * @param defaultValue The default property value is returned, if property is not already available
     * @return The value associated with key.
     */
    public String getProperty(String key, String defaultValue) {
        return properties.getOrDefault(key, defaultValue);
    }

    /**
     * This function returns the number of properties assigned to the task.
     * It should usually be used in combination with list and table
     * environments.
     *
     * @return Number of properties of the task
     */
    public int getPropertiesCount() {
        return properties.size();
    }

    /**
     * The <code>toString</code> function returns a string which combines the
     * reference code and it's name in a simple way. You may use this string
     * for a simple display of a task in a human readable format for example
     * in log files or similar.
     *
     * @return The reference code and name of the task as a single string
     */
    @Override
    public String toString() {
        return String.format("%s - %s", getReferenceCode(), getName());
    }
}
