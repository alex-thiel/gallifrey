package de.alx_development.gallifrey.tardis;

/*-
 * #%L
 * Gallifrey
 * %%
 * Copyright (C) 2021 ALX-Development
 * %%
 * This file is part of the de.alx_development.gallifrey application.
 * 
 * The gallifrey application is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Diese Datei ist Teil der de.alx_development.gallifrey Anwendung.
 * 
 * Die Applikation ist Freie Software: Sie können sie unter den
 * Bedingungen der GNU Lesser General Public License, wie von der
 * Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl)
 * jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren.
 * 
 * Diese Applikation wird in der Hoffnung, dass sie nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 * 
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2021 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import javax.swing.*;
import java.awt.*;

/**
 * The <code>TaskEditor</code> class provides a graphical interface for a
 * <code>Task</code> object and its properties
 */
public class TaskEditor extends JPanel {

    private Task task;
    private final JTable propertiesTable = new JTable();

    /**
     * Default constructor without any parameters
     */
    public TaskEditor() {
        super();

        /* Building the layout */
        setLayout(new BorderLayout());
        add(add(new JScrollPane(propertiesTable)), BorderLayout.CENTER);
    }

    /**
     * This function returns a reference to the actual assigned
     * <code>Task</code> object.
     *
     * @return Reference to the actual task
     */
    public Task getTask() {
        return task;
    }

    /**
     * Use this method to set or change the actually displayed <code>Task</code>
     * instance.
     *
     * @param task The task which should be displayed
     */
    public void setTask(Task task) {
        this.task = task;
        propertiesTable.setModel(new TaskPropertiesTableModel(getTask()));
    }
}
