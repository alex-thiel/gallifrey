package de.alx_development.gallifrey.tardis;

import javax.swing.table.AbstractTableModel;
import java.time.Duration;
import java.time.LocalDateTime;

public class TimeScheduleModel extends AbstractTableModel {

    private String[] columnNames = {"Task", "Beginn", "Ende", "Dauer"};

    private TimePeriod[] data = {
            new TimePeriod(new Task("6739.38383", "Testvorgang 2")),
            new TimePeriod(new Task("438573.130011", "Testvorgang 1")),
            new TimePeriod(new Task("534563.423483", "Testvorgang 3")),
            new TimePeriod(new Task("097890.38fd3", "Testvorgang 4")),
            new TimePeriod(new Task("123342.3fd83", "Testvorgang 5")),
            new TimePeriod(new Task("896843.3kl383", "Testvorgang 6")),
            new TimePeriod(new Task("54563.38sd3", "Testvorgang 7"))
    };

    public int getColumnCount() {
        return columnNames.length;
    }

    public int getRowCount() {
        return data.length;
    }

    public String getColumnName(int col) {
        return columnNames[col];
    }

    public Object getValueAt(int row, int col) {
        TimePeriod period = data[row];

        switch (col) {
            case 0:
                return period.getTask();
            case 1:
                return period.getStartTime();
            case 2:
                return period.getStopTime();
            case 3:
                return period.getDuration();
            default:
                throw new IllegalStateException("Unexpected value: " + col);
        }
    }

    public Class getColumnClass(int col) {
        switch (col) {
            case 0:
                return Task.class;
            case 1:
            case 2:
                return LocalDateTime.class;
            case 3:
                return Duration.class;
            default:
                throw new IllegalStateException("Unexpected value: " + col);
        }
    }

    /*
     * Don't need to implement this method unless your table's
     * editable.
     */
    public boolean isCellEditable(int row, int col) {
        //Note that the data/cell address is constant,
        //no matter where the cell appears onscreen.
        if (col < 2) {
            return false;
        } else {
            return true;
        }
    }

    /*
     * Don't need to implement this method unless your table's
     * data can change.
     */
    /*
    public void setValueAt(Object value, int row, int col) {
        data[row][col] = value;
        fireTableCellUpdated(row, col);
    }
    */
}
