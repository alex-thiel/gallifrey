package de.alx_development.gallifrey.tardis;

/*-
 * #%L
 * Gallifrey
 * %%
 * Copyright (C) 2021 ALX-Development
 * %%
 * This file is part of the de.alx_development.gallifrey application.
 * 
 * The gallifrey application is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Diese Datei ist Teil der de.alx_development.gallifrey Anwendung.
 * 
 * Die Applikation ist Freie Software: Sie können sie unter den
 * Bedingungen der GNU Lesser General Public License, wie von der
 * Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl)
 * jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren.
 * 
 * Diese Applikation wird in der Hoffnung, dass sie nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 * 
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2021 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import java.awt.*;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.table.TableCellRenderer;

public class TaskCellRenderer implements ListCellRenderer, TableCellRenderer {

    protected Logger logger = Logger.getLogger(getClass().getName());
    private final JLabel renderer = new JLabel();

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean hasFocus) {
        // The object value as task object (casted)
        Task task = (Task) value;

        // Determine if task is the active one in the given
        // list component. Always set this to false if no list
        // has been specified.
        boolean isActive;
        try {
            TaskListModel model = (TaskListModel)list.getModel();
            isActive = model.getActiveTask().equals(value);
        } catch(NullPointerException exception) {
            isActive = false;
        }

        // The label is return to be displayed
        JLabel renderer = getTaskRendererComponent(task, isSelected, hasFocus, isActive);
        renderer.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        return renderer;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        // The object value as task object (casted)
        Task task = (Task) value;

        // The label is return to be displayed
        JLabel renderer = getTaskRendererComponent(task, isSelected, hasFocus, false);

        // current table column width in pixels
        int colWidth = table.getColumnModel().getColumn(column).getWidth() + table.getIntercellSpacing().width;

        // set the text area width (height doesn't matter here)
        Dimension dim = new Dimension(colWidth, 1);
        renderer.setSize(dim);

        // get the text area preferred height and add the row margin
        int height = renderer.getPreferredSize().height + table.getRowMargin();


        // ensure the row height fits the cell with most lines
        if (height != table.getRowHeight(row)) {
            table.setRowHeight(row, height);
        }

        return renderer;
    }

    public JLabel getTaskRendererComponent(Task task, boolean isSelected, boolean cellHasFocus, boolean isActive) {

        // Must be triggered to get the <code>setBackground</code> command effective
        renderer.setOpaque(true);

        // Setting the text of the label and ignoring if now
        // task has been specified
        String text = "<html><p><b>Unknown</b></p></html>";
        try {
            text = "<html><p><b>"+task.getName()+"</b></p><p><small>"+task.getReferenceCode()+"</small></p></html>";
            // Setting the icon for the task type
            renderer.setIcon(task.getType());
        } catch(NullPointerException exception) {
            logger.warning("Task not fully specified: " + exception.getLocalizedMessage());
        }
        renderer.setText(text);

        // Task is actual active
        if(isActive){
            renderer.setBackground(Color.orange);
        }
        // Task is actually selected
        else if(isSelected){
            renderer.setForeground(UIManager.getColor("List.selectionForeground"));
            renderer.setBackground(UIManager.getColor("List.selectionBackground"));
        }
        // Task is not selected
        else{
            renderer.setForeground(UIManager.getColor("List.foreground"));
            renderer.setBackground(UIManager.getColor("List.background"));
        }

        // The label is return to be displayed
        return renderer;
    }
}
