package de.alx_development.gallifrey.tardis;

/*-
 * #%L
 * Gallifrey
 * %%
 * Copyright (C) 2021 ALX-Development
 * %%
 * This file is part of the de.alx_development.gallifrey application.
 * 
 * The gallifrey application is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Diese Datei ist Teil der de.alx_development.gallifrey Anwendung.
 * 
 * Die Applikation ist Freie Software: Sie können sie unter den
 * Bedingungen der GNU Lesser General Public License, wie von der
 * Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl)
 * jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren.
 * 
 * Diese Applikation wird in der Hoffnung, dass sie nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 * 
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2021 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import java.sql.*;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Logger;

public class Tardis implements TaskActivationListener {

    // Internal static reference to the single instance of
    // the T.A.R.D.I.S
    private static Tardis instance;

    protected Logger logger = Logger.getLogger(getClass().getName());
    private TimePeriod activePeriod = null;
    private Connection connection;
    private PreparedStatement taskUpdate;
    private PreparedStatement periodUpdate;
    private PreparedStatement periodSelect;

    /**
     * This function is the only access to the single
     * <code>Tardis</code> instance. Use this to get access to
     * a tardis reference.
     *
     * @return Reference to active tardis
     */
    public static Tardis getInstance () {
        if (Tardis.instance == null) {
            Tardis.instance = new Tardis();
        }
        return Tardis.instance;
    }

    /**
     * Private constructor which is internally used to instantiate a new
     * new tardis object. To get access to the active instance of the
     * tardis please use the #getInstance() function.
     *
     * @see #getInstance()
     */
    private Tardis() {
        logger.config("Creating a new T.A.R.D.I.S. instance for the vortex database");

        // Creating the database connection
        try {
            connection = DriverManager.getConnection("jdbc:derby:VortexDB;create=true");
            logger.config("Database connection established");

        } catch (SQLException exception) {
            logger.severe("Database connection failed: " + exception.getLocalizedMessage());
        }

        // Creating the tables if not already existing
        try {
            logger.fine("Creating the database tables");
            connection.prepareStatement("CREATE TABLE time_period (task_ref VARCHAR(50) NOT NULL, time_start TIMESTAMP NOT NULL, time_stop TIMESTAMP, PRIMARY KEY (task_ref, time_start))").execute();
            connection.prepareStatement("CREATE TABLE task (task_ref VARCHAR(50) NOT NULL, name VARCHAR(254), PRIMARY KEY (task_ref))").execute();
        } catch( SQLException e ) {
            if( "X0Y32".equals(e.getSQLState()) ) {
                logger.finest("The database tables are already prepared");
            }
            else {
                logger.severe("Database operation failed: " + e.getLocalizedMessage());
            }
        }

        // Creating the database statements
        try {
            periodUpdate = connection.prepareStatement("MERGE INTO time_period " +
                    "USING SYSIBM.SYSDUMMY1 " +
                    "ON time_period.task_ref = ? AND time_period.time_start = ? " +
                    "WHEN MATCHED THEN " +
                    "  UPDATE SET time_start = ?, time_stop = ? " +
                    "WHEN NOT MATCHED THEN " +
                    "  INSERT (task_ref, time_start, time_stop) " +
                    "  VALUES (?, ?, ?)");

            taskUpdate = connection.prepareStatement("MERGE INTO task " +
                    "USING SYSIBM.SYSDUMMY1 " +
                    "ON task.task_ref = ? " +
                    "WHEN MATCHED THEN " +
                    "  UPDATE SET name = ? " +
                    "WHEN NOT MATCHED THEN " +
                    "  INSERT (task_ref, name) " +
                    "  VALUES (?, ?)");

            periodSelect = connection.prepareStatement("SELECT task_ref, time_start, time_stop FROM time_period");

            logger.config("Database statements prepared and compiled");
        } catch (SQLException exception) {
            logger.severe("Database statement pre-compiling failed: " + exception.getLocalizedMessage());
        }
    }

    public String getHTMLString() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
        logger.finest("Reading database content");
        StringBuilder result = new StringBuilder("<html><head><style>\n" +
                "body {background-color: linen;}\n" +
                "table, th, td {border: 1px solid black;}\n" +
                "h1 {color: maroon; margin-left: 40px;}\n" +
                "</style></head><body><h1>Gallifrey Time Schedule</h1><table>" +
                "<tr><th>Task ID</th><th>Start</th><th>Stopp</th><th>Dauer</th></tr>");
        try {
            ResultSet rs = periodSelect.executeQuery();
            while(rs.next()){
                //Retrieve by column name
                String taskReference  = rs.getString("task_ref");
                LocalDateTime timeStart = rs.getTimestamp("time_start").toLocalDateTime();
                LocalDateTime timeStop = rs.getTimestamp("time_stop").toLocalDateTime();

                String line = "<tr><td>"+taskReference+"</td><td>"+timeStart.format(formatter)+"</td><td>"+timeStop.format(formatter)+"</td><td>"+ Duration.between(timeStart, timeStop).toSeconds() +" Sec.</td></tr>";
                result.append(line);
            }
            rs.close();
        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
        }
        result.append("</table></body></html>");
        return result.toString();
    }

    /**
     * This method is triggered by a task everytime new task is generated. In
     * this case the T.A.R.D.I.S. is storing the former active task in the database
     * as well as the new one, but without a termination timestamp.
     *
     * @param event The event containing information about the changed task activation.
     */
    @Override
    public void taskActivated(TaskActivationEvent event) {
        // Deactivating former period recording
        try {
            activePeriod.stop();
            // Updating the persistent storage
            storeTimePeriod(activePeriod);
            storeTask(event.getActivatedTask());
            logger.finest(String.format("Deactivated timer for [%s]", activePeriod.getTask().toString()));
            storeTask(event.getDeactivatedTask());
        }
        catch (NullPointerException exception) {
            logger.fine("Former period was already paused or not active");
        }

        // Setting up new period as active recording
        activePeriod = new TimePeriod(event.getActivatedTask());
        // Updating the persistent storage
        storeTimePeriod(activePeriod);
    }

    /**
     * Internally used function to store a <class>TimePeriod</class> object
     * persistent into the database
     *
     * @param period The <class>TimePeriod</class> object to store
     */
    private void storeTimePeriod(TimePeriod period) {
        // Updating the persistent storage
        try {
            logger.finest(String.format("Updating database storage for [%s]", period.getTask().toString()));

            // Updating the persistent storage
            periodUpdate.setString(1, period.getTask().getReferenceCode());
            periodUpdate.setObject(2, Timestamp.valueOf(period.getStartTime()));
            periodUpdate.setObject(3, Timestamp.valueOf(period.getStartTime()));
            periodUpdate.setObject(4, Timestamp.valueOf(LocalDateTime.now()));
            periodUpdate.setString(5, period.getTask().getReferenceCode());
            periodUpdate.setObject(6, Timestamp.valueOf(period.getStartTime()));
            periodUpdate.setObject(7, Timestamp.valueOf(LocalDateTime.now()));
            periodUpdate.execute();
        }
        catch (SQLException exception) {
            logger.fine("Database operation failed: " + exception.getLocalizedMessage());
        }
        catch (NullPointerException exception) {
            logger.fine("Former period was not active");
        }
    }

    /**
     * Internally used function to store a <class>Task</class> object
     * persistent into the database
     *
     * @param task The <class>Task</class> object to store
     */
    private void storeTask(Task task) {
        // Updating the persistent storage
        try {
            logger.finest(String.format("Updating database storage for task [%s]", task.getName()));

            // Updating the persistent storage
            taskUpdate.setString(1, task.getReferenceCode());
            taskUpdate.setString(2, task.getName());
            taskUpdate.setString(3, task.getReferenceCode());
            taskUpdate.setString(4, task.getName());
            taskUpdate.execute();
        }
        catch (SQLException exception) {
            logger.fine("Database operation failed: " + exception.getLocalizedMessage());
        }
    }

    /**
     * Closing the database connection and releasing the instance. This may
     * perhaps cause the garbage collector to determine the object.
     * If the <code>getInstance</code> method is invoked after the the
     * close operation, a new object will be generated and returned as reference.
     */
    public void close() {

        // Closing the last open task by providing a null reference
        // as new task object
        // Deactivating former period recording
        try {
            activePeriod.stop();
            logger.finest(String.format("Deactivating timer for [%s]", activePeriod.getTask().toString()));
            activePeriod = null;
        }
        catch (NullPointerException exception) {
            logger.fine("Former period was already paused or not active");
        }

        // Shutdown the database connection
        try {
            DriverManager.getConnection("jdbc:derby:;shutdown=true");
            instance = null;
        } catch (final SQLException e) {
            if ((e.getErrorCode() == 50000) && "XJ015".equals(e.getSQLState())) {
                logger.config("Database shutdown normally");
            } else {
                e.printStackTrace();
            }
        }
    }
}
