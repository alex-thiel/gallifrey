package de.alx_development.gallifrey.tardis;

/*-
 * #%L
 * Gallifrey
 * %%
 * Copyright (C) 2021 ALX-Development
 * %%
 * This file is part of the de.alx_development.gallifrey application.
 * 
 * The gallifrey application is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Diese Datei ist Teil der de.alx_development.gallifrey Anwendung.
 * 
 * Die Applikation ist Freie Software: Sie können sie unter den
 * Bedingungen der GNU Lesser General Public License, wie von der
 * Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl)
 * jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren.
 * 
 * Diese Applikation wird in der Hoffnung, dass sie nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 * 
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2021 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import de.alx_development.application.Application;

import javax.swing.*;
import java.awt.*;

public class TaskListView extends JPanel {

    private final TaskList taskList = new TaskList();

    public TaskListView() {

        /* Creating the task list panel */
        JScrollPane taskListPane = new JScrollPane(getTaskList());
        taskListPane.setMinimumSize(new Dimension(150, 20));
        taskListPane.setPreferredSize(new Dimension(350, 100));

        JButton timerStartButton = new JButton(new ImageIcon(getClass().getResource("timer-start.png")));
        timerStartButton.setToolTipText("Timer starten");
        timerStartButton.addActionListener(e -> {
            // Should only be active, if the actual active task is null (paused)
            if(getTaskList().getListModel().getActiveTask() == null) {
                Task lastActiveTask = getTaskList().getListModel().getLastActiveTask();
                Task selectedTask = getTaskList().getSelectedValue();
                // Restarting the former active task
                Application.logger.fine("Starting formerly active timer");
                getTaskList().getListModel().setActiveTask( (lastActiveTask != null) ? lastActiveTask : selectedTask );
            }
        });

        JButton timerStopButton = new JButton(new ImageIcon(getClass().getResource("timer-stop.png")));
        timerStopButton.setToolTipText("Timer stoppen");
        /* TODO: Implement action for stop button in toolbar */

        JButton timerPauseButton = new JButton(new ImageIcon(getClass().getResource("timer-pause.png")));
        timerPauseButton.setToolTipText("Timer pausieren");
        timerPauseButton.addActionListener(e -> {
            // Pausing the active timer is done by activating a null reference as
            // active task
            Application.logger.fine("Pausing active timer");
            getTaskList().getListModel().setActiveTask(null);
        });

        JButton taskActivateButton = new JButton(new ImageIcon(getClass().getResource("task-activate.png")));
        taskActivateButton.setToolTipText("Selektierten Task aktivieren");
        taskActivateButton.addActionListener(e -> {
            try {
                Task task = getTaskList().getSelectedValue();
                Application.logger.fine(String.format("Try activating task <%s>", task.toString()));
                getTaskList().getListModel().setActiveTask(task);
            } catch(NullPointerException exception) {
                Application.logger.warning("Nothing suitable selected for activation");
            }
        });

        final JToolBar toolBar = new JToolBar("Still draggable");
        toolBar.setBorderPainted(true);
        toolBar.add(timerStartButton);
        toolBar.add(timerPauseButton);
        toolBar.add(timerStopButton);
        toolBar.addSeparator();
        toolBar.add(taskActivateButton);

        /* Building the layout */
        setLayout(new BorderLayout());
        add(toolBar, BorderLayout.NORTH);
        add(taskListPane, BorderLayout.CENTER);

    }

    public TaskList getTaskList() {
        return taskList;
    }
}
