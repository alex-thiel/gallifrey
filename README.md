![Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/alex-thiel/gallifrey/master)
![Bitbucket open issues](https://img.shields.io/bitbucket/issues-raw/alex-thiel/gallifrey)

# Gallifrey - Time recording and management system #

Gallifrey ist ein System welches dem Anwender erleichtern soll den Überblick über Arbeitszeiten die auf verschiedene Projekte verteilt sind zu behalten.

Wer kennt das nicht aus der täglichen Arbeit? Man will konzentriert an einer Aufgabe arbeiten, wurde jedoch immer mal wieder durch Kollegen oder andere Aufgaben herausgerissen. Am Ende des Tages fällt es dann schwer zu sagen, wieviel Zeit man wirklich am eigentlichen Projekt verbracht hat. Insbesondere "kurze" Unterbrechungen wie Telefonanrufe machen es schwer die Zeiten im Blick zu behalten.

Hier kann Gallifrey helfen. Das Programm schreibt die Zeiten der aktuell aktivierten Aufgabe im Hintergrund automatisch mit. Nur ein Klick ermöglicht es auf eine andere Aufgabe umzuschalten und später schnell auch wieder zurück. So ist es auch möglich kurze Unterbrechungen von wenigen Minuten exakt zu erfassen und einen genauen Überblick der eigenen Arbeitszeit zu behalten.

### Kontakt? ###

Eigene Ideen oder Bugs gefunden? Bitte meldet euch, ich freue mich über eure Mitarbeit.

* alex@thiel-online.net
